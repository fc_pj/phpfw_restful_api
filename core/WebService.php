<?php 
class WebService
{
    public function getUrlContent( $link = "", $options = array(), $headers = array(),  $post_data = array() ){
        $return_arr = array(
            'error' => TRUE,
            'link' => '',
            'status' => 0,
            'content' => '',
            'process_time' => ''
        );

        $cConnection = curl_init();

        curl_setopt($cConnection, CURLOPT_URL, $link); 
        curl_setopt($cConnection, CURLOPT_USERAGENT, $this->_getRandomUserAgent()); 
        if ( is_array($headers) && count($headers) > 0 ) {
            curl_setopt($cConnection, CURLOPT_HTTPHEADER, $headers);
        }
        // Post request
        if ( is_array($post_data) && count($post_data) > 0 ) {
            curl_setopt($cConnection, CURLOPT_POSTFIELDS, $post_data);
        }
        // if ( is_array($options) && count($options) > 0 ) {
        //     var_dump($options);
        //     curl_setopt_array($cConnection, $options);
        // }
        // // return raw data
        curl_setopt($cConnection, CURLOPT_RETURNTRANSFER, 1);
        // Set 0 to skip SSL check
        curl_setopt($cConnection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($cConnection, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($cConnection, CURLOPT_CONNECTTIMEOUT, $timeout);
        $output = curl_exec($cConnection);
        if( !curl_errno($cConnection) ){
            // cUrl return info
            $info = curl_getinfo($cConnection);
            $return_arr['error'] = FALSE;
            $return_arr['link'] = $info['url'];
            $return_arr['process_time'] = $info['total_time'];
            $return_arr['content'] = $output;
            $return_arr['status'] = $info['http_code'];
        }
        curl_close($cConnection);
        // var_dump($return_arr);
        return $return_arr;
    }

    private function _getRandomUserAgent()
    {
        $userAgents = array(
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)",
            "Opera/9.20 (Windows NT 6.0; U; en)",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; en) Opera 8.50",
            "Mozilla/4.0 (compatible; MSIE 6.0; MSIE 5.5; Windows NT 5.1) Opera 7.02 [en]",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; fr; rv:1.7) Gecko/20040624 Firefox/0.9",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/48 (like Gecko) Safari/48" 
        );
        $random = rand(0,count($userAgents)-1);

        return $userAgents[$random];
    }
}