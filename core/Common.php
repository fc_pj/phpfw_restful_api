<?php 
class Common
{
    public function returnFormat( $data, $format ){
        $return_data = is_array($data) ? $data : array();
        $return_format = isset($format{0}) ? strtolower($format) : 'json';
        # Default return array
        $return_arr = array(
            'error' => isset($return_data['error']) ? $return_data['error'] : TRUE,
            'data' => isset($return_data['content']) ? $return_data['content'] : 'NONE',
            'status' => isset($return_data['status']) ? $return_data['status'] : 404,
            // 'msg' => 'Empty return data',
            'format' => $return_format
        );

        switch( $format ){
            case 'json':
                $return_arr['error'] = FALSE;
                break;

            case 'xml':
                $xml_str = $this->_md_array_restructure($return_arr);
                $return_arr['error'] = FALSE;
                $return_arr['data'] = $xml_str;
                break;
        }
        return $return_arr;
    }

    # include multi-dimensional array restructure
    private function _md_array_restructure( $array, $recursive = FALSE ){
        $xml_str = (!$recursive) ? '<?xml version="1.0" encoding="UTF-8"?>' : '';
        foreach( $array As $key => $value ){
            # if this is multi-dimensional array
            if( is_array($value) ){
                $xml_str .= "<$key>".$this->_md_array_restructure($value, TRUE)."</$key>";
            }else{
                $xml_str .= "<$key>$value</$key>";
            }
        }
        return $xml_str;
    }
}