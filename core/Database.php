<?php
class Database{
    private $pdo;
    protected $return_arr = array(
        'error' => TRUE,
        'connect' => FALSE,
        'query' => '',
        'data' => array(),
        'msg' => ''
    );

    function __construct() {
        if($this->return_arr['connect'] === TRUE){
            $this->return_arr['error'] = FALSE;
        }else{
            try {
                $this->pdo = new PDO("mysql:host=".CONFIG::DB_SER.";dbname=".CONFIG::DB_TABLE, CONFIG::DB_USER, CONFIG::DB_PW);
                $this->pdo->exec("SET CHARACTER SET utf8");
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->return_arr['error'] = FALSE;
                $this->return_arr['connect'] = TRUE;
            } catch (PDOException $e) {
                $this->return_arr['error'] = TRUE;
                $this->return_arr['connect'] = FALSE;
                $this->return_arr['msg'] = $e->getMessage();
            }
        }
    }

    public function select( $option = array() ){
        if( !$this->return_arr['connect'] ){
            return $this->return_arr;
        }
        $this_query_arr = $this->_sql_query_builder('SELECT', $option);
        if(!$this_query_arr['error'] ){
            try {
                $stmt = $this->pdo->prepare($this_query_arr['query']);
                $stmt->execute();
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $this->return_arr['error'] = FALSE;
                $this->return_arr['query'] = $this_query_arr['query'];
                $this->return_arr['data'] = $results;
                $this->return_arr['msg'] = 'Success';
            } catch (PDOException $e) {
                $this->return_arr['error'] = TRUE;
                $this->return_arr['query'] = $this_query_arr['query'];
                $this->return_arr['msg'] = $e->getMessage();
            }
        }
        return $this->return_arr;
    }

    public function insert( $option = array() ){
        if( !$this->return_arr['connect'] ){
            return $this->return_arr;
        }
        $this_query_arr = $this->_sql_query_builder('INSERT', $option);
        if(!$this_query_arr['error'] ){
            $total_record = 0;
            try {
                $this->pdo->beginTransaction();
                if( count($this_query_arr['query']) > 0 ){
                    foreach( $this_query_arr['query'] As $tbl_name => $query_arr){
                        if( count($query_arr['data']) > 0 ){
                            $stmt = $this->pdo->prepare($query_arr['query']);
                            foreach( $query_arr['data'] As $bind_datas ){
                                foreach( $bind_datas As $bind_key => $bind_data ){
                                    $stmt->bindValue($bind_key,$bind_data);
                                }
                                $total_record++;
                                $stmt->execute();
                            }
                        }
                    }
                }
                $this->pdo->commit();
                $this->return_arr['error'] = FALSE;
                $this->return_arr['query'] = $this_query_arr['query'];
                $this->return_arr['msg'] = 'Insert {'.count($this_query_arr['query']).'} table(s), total {'.$total_record.'} item(s) Success';
            } catch (PDOException $e) {
                $this->pdo->rollback();
                $this->return_arr['error'] = TRUE;
                $this->return_arr['query'] = 'Insert Error';
                $this->return_arr['msg'] = $e->getMessage();
            }
        }
        return $this->return_arr;
    }

    public function update( $option = array() ){
        if( !$this->return_arr['connect'] ){
            return $this->return_arr;
        }
        $this_query_arr = $this->_sql_query_builder('UPDATE', $option);
        if(!$this_query_arr['error'] ){
            $total_record = 0;
            try {
                $this->pdo->beginTransaction();
                if( count($this_query_arr['query']) > 0 ){
                    foreach( $this_query_arr['query'] As $tbl_name => $query_arr){
                        if( count($query_arr['data']) > 0 ){
                            $stmt = $this->pdo->prepare($query_arr['query']);
                            foreach( $query_arr['data'] As $bind_datas ){
                                foreach( $bind_datas As $bind_key => $bind_data ){
                                    $stmt->bindValue($bind_key,$bind_data);
                                }
                                $total_record++;
                                $stmt->execute();
                            }
                        }
                    }
                }
                $this->pdo->commit();
                $this->return_arr['error'] = FALSE;
                $this->return_arr['query'] = $this_query_arr['query'];
                $this->return_arr['msg'] = 'Update {'.count($this_query_arr['query']).'} table(s), total {'.$total_record.'} item(s) Success';
            } catch (PDOException $e) {
                $this->pdo->rollback();
                $this->return_arr['error'] = TRUE;
                $this->return_arr['query'] = 'Insert Error';
                $this->return_arr['msg'] = $e->getMessage();
            }
        }
        return $this->return_arr;
    }
    
    public function delete( $option = array() ){
        if( !$this->return_arr['connect'] ){
            return $this->return_arr;
        }
        $this_query_arr = $this->_sql_query_builder('DELETE', $option);
        if(!$this_query_arr['error'] ){
            $total_record = 0;
            try {
                $this->pdo->beginTransaction();
                echo count($this_query_arr['query']);
                if( count($this_query_arr['query']) > 0 ){
                    foreach( $this_query_arr['query'] As $tbl_name => $query_arr){
                        echo count($query_arr['data']);
                        if( count($query_arr['data']) > 0 ){
                            echo $query_arr['query'];
                            $stmt = $this->pdo->prepare($query_arr['query']);
                            foreach( $query_arr['data'] As $bind_datas ){
                                foreach( $bind_datas As $bind_key => $bind_data ){
                                    $stmt->bindValue($bind_key,$bind_data);
                                }
                                $total_record++;
                                $stmt->execute();
                            }
                        }
                    }
                }
                $this->pdo->commit();
                $this->return_arr['error'] = FALSE;
                $this->return_arr['query'] = $this_query_arr['query'];
                $this->return_arr['msg'] = 'Delete {'.count($this_query_arr['query']).'} table(s), total {'.$total_record.'} item(s) Success';
            } catch (PDOException $e) {
                $this->pdo->rollback();
                $this->return_arr['error'] = TRUE;
                $this->return_arr['query'] = 'Delete Error';
                $this->return_arr['msg'] = $e->getMessage();
            }
        }
        return $this->return_arr;
    }

    private function _sql_query_builder( $type = '', $option = array() ){
        $return_arr = array(
            'error' => TRUE,
            'query' => "",
            'msg' => ''
        );

        $query_type = isset($type{0}) ? strtoupper($type) : 'SELECT';
        $selects = isset($option['select']{0}) ? $option['select'] : '*';
        $select_sql = '';
        $tbl_name = isset($option['table']{0}) ? " ".$option['table'] : '';
        $where_arr = isset($option['where']) ? $option['where'] : array();
        $or_where_arr = isset($option['or_where']) ? $option['or_where'] : array();
        $where_sql = '';
        $group_by =  isset($option['group_by']{0}) ? ' GROUP BY '.$option['group_by'] : '';
        $order_by =  isset($option['order_by']{0}) ? ' ORDER BY '.$option['order_by'] : '';

        if( isset($tbl_name{0}) ){
            if( is_array($selects) ){
                foreach( $selects As $select ){
                    $select_sql = isset($select_sql{0}) ? $select_sql.", ".$select : " ".$select;
                }
            }else{
                $select_sql = " ".$selects;
            }

            if( is_array($where_arr) ){
                foreach( $where_arr As $where){
                    $where_sql = isset($where_sql{0}) ? $where_sql.' AND '.$where : $where_sql.$where;
                }
            }
            if( is_array($or_where_arr) ){
                foreach( $or_where_arr As $where){
                    $where_sql = isset($where_sql{0}) ? $where_sql .' OR '.$where : $where_sql.$where;
                }
            }
            $where_sql = isset($where_sql{0}) ? ' WHERE '.$where_sql : $where_sql;

            switch($query_type){
                case "SELECT":
                    # SELECT * FROM TBL 
                    # WHERE A=A
                    # GROUP BY A
                    # ORDER BY A ASC
                    $query = $query_type.$select_sql." FROM".$tbl_name;
                    $query .= $where_sql;
                    $query .= $group_by;
                    $query .= $order_by;
                    $return_arr['error'] = FALSE;
                    $return_arr['query'] = $query;
                    break;
                case "UPDATE":
                    # UPDATE TBL SET A=:A WHERE ID=:ID
                    if( count($option['data']) > 0 ){
                        $return_arr['query'] = array();
                        $return_query = array();
                        foreach( $option['data'] As $data ){
                            $update_sql = '';
                            $values_arr = array();
                            $target_tbl = $tbl_name;
                            $where_sql = '';
                            foreach( $data As $col_name => $value ){
                                if( $col_name === '__TBL_NAME__' ){
                                    $target_tbl = $value;
                                }else{
                                    if( !array_key_exists($target_tbl, $return_query) ){
                                        $return_query[$target_tbl] = array();
                                        $return_query[$target_tbl]['query'] = '';
                                        $return_query[$target_tbl]['data'] = array();
                                    }
                                    $bind_name = ':'.$col_name;
                                    if( $col_name === '__WHERE__' ){
                                        if( is_array($value) && count($value) > 0 ){
                                            $where_unique = array_unique($value);
                                            foreach( $where_unique As $where_name => $where_value ){
                                                $bind_name = ':WHERE_'.$where_value;
                                                $where_sql = isset($where_sql{0}) ? $where_sql.' AND '.$where_value.' = '.$bind_name : $where_value.' = '.$bind_name;
                                                if( array_key_exists($where_value, $data) ){
                                                    $values_arr[$bind_name] = $data[$where_value];
                                                }else{
                                                    $return_arr['error'] = TRUE;
                                                    $return_arr['query'] = array();
                                                    $return_arr['msg'] = 'Update table {'.$target_tbl.'} have not provide {'.$where_value.'} value(s)';
                                                    return $return_arr;
                                                }
                                            }
                                        }
                                    }else{
                                        $update_sql = isset($update_sql{0}) ? $update_sql.', '.$col_name.' = '.$bind_name : $col_name .' = '.$bind_name;
                                        $values_arr[$bind_name] = $value;    
                                    }
                                }
                            }
                            $query = 'UPDATE '.$target_tbl.' SET '.$update_sql.' WHERE '.$where_sql."\n";
                            array_push($return_query[$target_tbl]['data'], $values_arr);
                            $return_query[$target_tbl]['query'] = $query;
                            if( strlen($update_sql) <= 0 ){
                                $return_arr['error'] = TRUE;
                                $return_arr['query'] = array();
                                $return_arr['msg'] = 'Update Error';
                            }else{
                                $return_arr['error'] = FALSE;
                            }
                        }
                        $return_arr['query'] = $return_query;
                    }
                    break;
                case "INSERT":
                    #  INSERT INTO TBL (COL1, COL2) VALUES (INFO1, INFO2)
                    if( count($option['data']) > 0 ){
                        $return_arr['query'] = array();
                        $return_query = array();
                        foreach( $option['data'] As $data ){
                            $col_names = '';
                            $values = '';
                            $values_arr = array();
                            $target_tbl = $tbl_name;
                            foreach( $data As $col_name => $value ){
                                if( $col_name === '__TBL_NAME__' ){
                                    $target_tbl = $value;
                                }else{
                                    if( !array_key_exists($target_tbl, $return_query) ){
                                        $return_query[$target_tbl] = array();
                                        $return_query[$target_tbl]['query'] = '';
                                        $return_query[$target_tbl]['data'] = array();
                                    }
                                    $col_names = isset($col_names{0}) ? $col_names.','.$col_name : $col_name;
                                    $bind_name = ':'.$col_name;
                                    $values = isset($values{0}) ? $values.', '.$bind_name : $bind_name;
                                    $values_arr[$bind_name] = $value;    
                                }
                            }
                            array_push($return_query[$target_tbl]['data'], $values_arr);
                            $return_query[$target_tbl]['query'] = 'INSERT INTO '.$target_tbl.' ('.$col_names.') VALUES ('.$values.')';
                            if( strlen($col_names) <= 0 || strlen($values) <= 0 ){
                                $return_arr['error'] = TRUE;
                                $return_arr['query'] = array();
                                $return_arr['msg'] = 'Insert Error';
                            }else{
                                $return_arr['error'] = FALSE;
                            }
                        }
                        $return_arr['query'] = $return_query;
                    }
                    break;
                case "DELETE":
                # DELETE FROM TBL WHERE A=:A
                    if( count($option['data']) > 0 ){
                        $return_arr['query'] = array();
                        $return_query = array();

                        foreach( $option['data'] As $datas ){
                            $target_tbl = $tbl_name;
                            $values_arr = array();
                            $where_sql = '';
                            foreach( $datas As $col_name => $col_value){
                                if( $col_name === '__TBL_NAME__' ){
                                    $target_tbl = $col_value;
                                }else{
                                    if( !array_key_exists($target_tbl, $return_query) ){
                                        $return_query[$target_tbl] = array();
                                        $return_query[$target_tbl]['query'] = '';
                                        $return_query[$target_tbl]['data'] = array();
                                    }
                                        $bind_name = ':WHERE_'.$col_name;
                                        $where_sql = isset($where_sql{0}) ? $where_sql.' AND '.$col_name.' = '.$bind_name : ' WHERE '.$col_name.' = '.$bind_name;
                                        $values_arr[$bind_name] = $col_value;
                                        if( count($values_arr) > 0 ){
                                            array_push($return_query[$target_tbl]['data'], $values_arr);
                                        }
                                }
                            }
                            $return_query[$target_tbl]['query'] = 'DELETE FROM '.$target_tbl.$where_sql;
                        }
                        $return_arr['error'] = FALSE;
                        $return_arr['query'] = $return_query;
                    }
                    break;
            }
        }else{
            $return_arr['error'] = TRUE;
            $return_arr['msg'] = 'No table name';
        }
        return $return_arr;
    }

}