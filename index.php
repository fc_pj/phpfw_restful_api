<?php 
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once(__DIR__ .'/config.php');
require_once(__DIR__ .'/core/HttpStatus.php');
require_once(__DIR__ .'/core/Common.php');
$hs = new HttpStatus();
$calling_class = ( isset($_REQUEST['cc']{0}) ) ? $_REQUEST['cc'] : 'NONE';
$calling_function = ( isset($_REQUEST['cf']{0}) ) ? $_REQUEST['cf'] : 'index';
if( $calling_class === 'NONE' ){
    $hs->show_error('html', 'Do not have this Class - '.$calling_class);
    exit();
}

require_once(__DIR__ .'/library/'. $calling_class .'.php');
$classObj = new $calling_class($_REQUEST);

if( !method_exists($classObj,$calling_function) ){
    $hs->show_error('html', 'Do not have this Function - '.$calling_function);
    exit();
}

$common = new Common();

$return_type = 'json';
$result = $common->returnFormat($classObj->$calling_function(), $return_type);
$hs->setHttpStatus($result['status'], $return_type);
echo $result['data'];
