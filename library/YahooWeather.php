<?php
require_once(__DIR__ .'\..\core\WebService.php');

class YahooWeather{
    public $yahoo_weather_url = "https://weather-ydn-yql.media.yahoo.com/forecastrss";
    public $params = array(
        'location'=> 'Sydney, Australia',
        'format'=> 'json',
        'u'=> 'c'
    );   
    
    function __construct($request_arr = array() ) {
        // var_dump($request_arr);
        $this->params['location'] = ( isset($request_arr['location']) ) ? $request_arr['location'] : $this->params['location'];
        $this->params['format'] = ( isset($request_arr['format']) ) ? $request_arr['format'] : $this->params['format'];
        $this->params['u'] = ( isset($request_arr['u']) ) ? $request_arr['u'] : $this->params['u'];
    }

    public function index(){
        // Get yahoo weather info
        $ywData = $this->queryData();
        $ws = new WebService();
        $webResult = $ws->getUrlContent($ywData['url'], array(), $ywData['header']);
        if( $webResult['error'] == FALSE ){
            return $webResult;
        }
    }

    public function queryData(){

        $oauth = array(
            'oauth_consumer_key' => CONFIG::YW_CONSUMER_KEY,
            'oauth_nonce' => uniqid(mt_rand(1, 1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );
        $base_info = $this->_buildBaseString($this->yahoo_weather_url, 'GET', array_merge($this->params, $oauth));
        $composite_key = rawurlencode(CONFIG::YW_CONSUMER_SECRET) . '&';
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        $header = array(
            $this->_buildAuthorizationHeader($oauth),
            'X-Yahoo-App-Id: ' . CONFIG::YW_APP_ID
        );

        $return_arr['error'] = FALSE;
        $return_arr['url'] = $this->yahoo_weather_url . '?' . http_build_query($this->params);
        $return_arr['header'] = $header;
        return $return_arr;
    }

    private function _buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    private function _buildAuthorizationHeader($oauth) {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach($oauth as $key=>$value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }
}
