<?php
require_once(__DIR__ .'\..\core\Database.php');

class DBTest{
    public function select() {
        $db = new Database();

        # Select
        $select_option = array(  
            'select' => 'id, username', 
            'table' => 'user1', 
            'where' => array( 
            'username' => 'A',
        ),
        'or_where' => array(  
            'username' => 'B',
        ) 
        );
        var_dump($db->select($select_option));
    }
    
    public function insert() {
        $db = new Database();
        # Insert
        $insert_option = array(
            'table' => 'user1',
            'data' => array(
                array(
                    '__TBL_NAME__' => 'user2',
                    'name1' => 'User2_Name1_1',
                    'name2' => 'User2_Name2_1'
                ),
                array(
                    '__TBL_NAME__' => 'user2',
                    'name1' => 'User2_Name1_2',
                    'name2' => 'User2_Name2_2'
                ),
                array(
                    'username' => 'A',
                    'nickname' => 'Apple'
                ),
                array(
                    'username' => 'B',
                    'nickname' => 'Banana'
                ),
                array(
                    'username' => 'C',
                    'nickname' => 'Crossover'
                )
            )
        );
        var_dump($db->insert($insert_option));
    }

    public function update() {
        $db = new Database();
        # Update
        $update_option = array(
            'table' => 'user1',
            'data' => array(
                array(
                    'id' => 1,
                    'username' => 'Rename_A',
                    'nickname' => 'Remane_Apple',
                    '__WHERE__' => array('id')
                ),
                array(
                    'id' => 3,
                    'username' => 'C',
                    'nickname' => 'Rename_Crossover',
                    '__WHERE__' => array('id')
                ),
                array(
                    '__TBL_NAME__' => 'user2',
                    'id' => 1,
                    'name1'=>'User2_Name1_1R',
                    'name2'=>'User2_Name2_1R',
                    '__WHERE__' => array('id')
                ),
                array(
                    '__TBL_NAME__' => 'user2',
                    'id' => 2,
                    'name1'=>'User2_Name1_2R',
                    'name2'=>'User2_Name2_2R',
                    '__WHERE__' => array('id')
                )
            )
        );
        var_dump($db->update($update_option));
    }

    public function delete() {
        $db = new Database();
        # Delete
        $delete_option = array(
            'table' => 'user1',
            'data' => array(
                array(
                    'id' => 1
                ),
                array(
                    '__TBL_NAME__' => 'user2',
                    'id' => 1
                ),
                array(
                    '__TBL_NAME__' => 'user2',
                    'id' => 2
                )
            )
        );
        var_dump($db->delete($delete_option));
    }
}