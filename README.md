Develop environment:   
OS: IIS10 0.17763.1  
PHP: 7.2.7  

Core Library:  
library\core\Common.php  
library\core\HttpStatus.php  
library\core\WebService.php  
library\core\Database.php  

DB class - Select  
---
SQL: SELECT A1,A2 FROM ABC WHERE KEY1 = 'A' AND KEY2 = 'B' OR KEY3='C' OR KEY4 = 'D'  
$db = new Database();  
$option = array(  
    'select' => 'A1, A2',  
    'table' => 'ABC',  
    'where' => array(  
        'KEY1' => 'A',  
        'KEY2' => 'B'  
    ),  
    'or_where' => array(  
        'KEY3' => 'C',  
        'KEY4' => 'D'  
    )  
);  
var_dump($db->select($option));  

DB class - Insert  
---
SQL1: INSERT INTO TEST (KEY1,KEY2) VALUES (V1,V2) AND   
SQL2: INSERT INTO user (KEY3,KEY4) VALUES (V3,V4)  
$db = new Database();
$option = array(  
    'table' => 'user',  
    'data' => array(  
        array(  
        '__TBL_NAME__' => 'TEST',  
        'KEY1' => 'V1',  
        'KEY2' => 'V2'  
        ),  
        array(  
            'KEY3' => 'V3',  
            'KEY4' => 'V4'  
        )  
    )  
);
var_dump($db->insert($option));  
*Add coloumn name "__TBL_NAME__" to set target table. Insert two tables in one insert command

DB class - Update  
---
SQL1: INSERT INTO TEST (KEY1,KEY2) VALUES (V1,V2) AND   
SQL2: INSERT INTO user (KEY3,KEY4) VALUES (V3,V4)  
$db = new Database();
$option = array(  
    'table' => 'user',  
    'data' => array(  
        array(  
        '__TBL_NAME__' => 'TEST',  
        'KEY1' => 'V1',  
        'KEY2' => 'V2'  
        ),  
        array(  
            'KEY3' => 'V3',  
            'KEY4' => 'V4'  
        )  
    )  
);
var_dump($db->insert($option));  
*Add coloumn name "__TBL_NAME__" to set target table. Insert two tables in one insert command