<?php 
class Config{
    // Yahoo Weather API details
    const YW_APP_ID = 'Yahoo Weather APP ID';
    const YW_CONSUMER_KEY = 'Yahoo Weather Customer Key';
    const YW_CONSUMER_SECRET = 'Yahoo Weather Secret';

    // Database Setting
    const DB_SER = 'localhost';
    const DB_USER = 'USER';
    const DB_PW = 'PASSWORD';
    const DB_TABLE = 'DB';
}